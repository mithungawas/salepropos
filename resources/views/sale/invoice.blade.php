<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" type="image/png" href="{{url('public/logo', $general_setting->site_logo)}}" />
    <title>{{$general_setting->site_title}}</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">

    <style type="text/css">
        * {
            font-size: 14px;
            /*line-height: 24px;*/
            font-family: "Roboto", sans-serif;
            text-transform: capitalize;
        }
        .btn {
            padding: 7px 10px;
            text-decoration: none;
            border: none;
            display: block;
            text-align: center;
            margin: 7px;
            cursor:pointer;
        }

        .btn-info {
            background-color: #999;
            color: #FFF;
        }

        .btn-primary {
            background-color: #6449e7;
            color: #FFF;
            width: 100%;
        }
        td,
        th,
        tr,
        table {
            border-collapse: collapse;
        }
        tr {border-bottom: 1px dotted #ddd;}
        td,th {padding: 2px 0;width: 50%;}

        table {width: 100%;}
        tfoot tr th:first-child {text-align: left;}

        .centered {
            text-align: center;
            align-content: center;
        }
        small{font-size:11px;}
        .shop-name{
            font-weight: bold;
        }
        hr.dashed{
            border-top: 1px dotted grey;
        }

        @media print {
            * {
                font-size:16px;
                line-height: 15px;
            }
            td,th {padding: 2px 0;}
            .hidden-print {
                display: none !important;
            }
            @page { margin: 0; } body { margin-bottom:0.5cm; }
        }
    </style>
  </head>
<body>

<div style="max-width: 250px;margin:0 auto">
    @if(preg_match('~[0-9]~', url()->previous()))
        @php $url = '../../pos'; @endphp
    @else
        @php $url = url()->previous(); @endphp
    @endif
    <div class="hidden-print">
        <table>
            <tr>
                <td><a href="{{$url}}" class="btn btn-info"><i class="fa fa-arrow-left"></i> {{trans('file.Back')}}</a> </td>
                <td><button onclick="window.print();" class="btn btn-primary"><i class="fa fa-print"></i> {{trans('file.Print')}}</button></td>
            </tr>
        </table>
        <br>
    </div>
    <div id="receipt-data">
        <div class="centered">
            @if($general_setting->site_logo)
                <img src="{{url('public/logo', $general_setting->site_logo)}}" height="75" style="margin:10px 0;filter: brightness(0);">
            @endif
            
            <div class="shop-name">{{$lims_biller_data->company_name}}</div>
            {{$lims_warehouse_data->address}}
                <br>{{$lims_warehouse_data->phone}}

        </div>
        <p style="margin-bottom: 0">
            {{trans('file.Date')}}: {{$lims_sale_data->created_at}}<br>
            {{trans('file.reference')}}: {{$lims_sale_data->reference_no}}<br>
            {{trans('file.customer')}}: {{$lims_customer_data->name}}
        </p>
        <hr class="dashed" />
        <table>
            <tbody>
                @foreach($lims_product_sale_data as $product_sale_data)
                @php 
                    $lims_product_data = \App\Product::find($product_sale_data->product_id);
                    if($product_sale_data->variant_id) {
                        $variant_data = \App\Variant::find($product_sale_data->variant_id);
                        $product_name = $lims_product_data->name.' ['.$variant_data->name.']';
                    }
                    else
                        $product_name = $lims_product_data->name;
                @endphp
                <tr>
                    <td>{{ str_limit($product_name, $limit = 10, $end = '...') }}</td>
                    <td>{{$product_sale_data->qty}}x{{number_format((float)($product_sale_data->total / $product_sale_data->qty), 2, '.', '')}}</td>
                    <td style="text-align:right; font-weight: bold">{{number_format((float)$product_sale_data->total, 2, '.', '')}}</td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th>{{trans('file.Total')}}</th>
                    <th colspan="2" style="text-align:right">{{$general_setting->currency}}{{number_format((float)$lims_sale_data->total_price, 2, '.', '')}}</th>
                </tr>
                @if($lims_sale_data->order_tax)
                <tr>
                    <th>{{trans('file.Order Tax')}}</th>
                    <th colspan="2" style="text-align:right">{{$general_setting->currency}}{{number_format((float)$lims_sale_data->order_tax, 2, '.', '')}}</th>
                </tr>
                @endif
                @if($lims_sale_data->order_discount)
                <tr>
                    <th>{{trans('file.Order Discount')}}</th>
                    <th colspan="2" style="text-align:right">{{number_format((float)$lims_sale_data->order_discount, 2, '.', '')}}</th>
                </tr>
                @endif
                @if($lims_sale_data->coupon_discount)
                <tr>
                    <th>{{trans('file.Coupon Discount')}}</th>
                    <th colspan="2" style="text-align:right">{{$general_setting->currency}}{{number_format((float)$lims_sale_data->coupon_discount, 2, '.', '')}}</th>
                </tr>
                @endif
                @if($lims_sale_data->shipping_cost)
                <tr>
                    <th>{{trans('file.Shipping Cost')}}</th>
                    <th colspan="2" style="text-align:right">{{$general_setting->currency}}{{number_format((float)$lims_sale_data->shipping_cost, 2, '.', '')}}</th>
                </tr>
                @endif
                <tr>
                    <th>{{trans('file.grand total')}}</th>
                    <th colspan="2" style="text-align:right">{{$general_setting->currency}}{{number_format((float)$lims_sale_data->grand_total, 2, '.', '')}}</th>
                </tr>
                <tr>
                    @if($general_setting->currency_position == 'prefix')
                    <th class="centered" colspan="3">{{trans('file.In Words')}}: <span>{{str_replace("-"," ",$numberInWords)}} Rupees only</span></th>
                    @else
                    <th class="centered" colspan="3">{{trans('file.In Words')}}: <span>{{str_replace("-"," ",$numberInWords)}}</span> <span>{{$general_setting->currency}}</span></th>
                    @endif
                </tr>
            </tfoot>
        </table>
        <table>
            <tbody>
                @foreach($lims_payment_data as $payment_data)
                    @if($payment_data->change != 0)
                        <tr>
                            <td style="padding: 5px;width:30%">{{trans('file.Paid By')}}: {{$payment_data->paying_method}}</td>
                            <td style="padding: 5px;width:40%">{{trans('file.Amount')}}: {{number_format((float)$payment_data->amount, 2, '.', '')}}</td>
                            <td style="padding: 5px;width:30%">{{trans('file.Change')}}: {{number_format((float)$payment_data->change, 2, '.', '')}}</td>
                        </tr>
                    @else
                        <tr>
                            <td style="padding: 5px;width:30%">{{trans('file.Paid By')}}: {{$payment_data->paying_method}}</td>
                            <td style="padding: 5px;width:40%">{{trans('file.Amount')}}: {{number_format((float)$payment_data->amount, 2, '.', '')}}</td>
                        </tr>
                    @endif
                <tr><td class="centered" colspan="3">{{trans('file.Thank you for shopping with us. Please come again')}}</td></tr>
                @endforeach
            </tbody>
        </table>
        @if($lims_sale_data->reference_no)
            <div class="centered">
                <svg class="barcode"
                     jsbarcode-format="CODE128"
                     jsbarcode-value="{{$lims_sale_data->reference_no}}"
                     jsbarcode-textmargin="0"
                     jsbarcode-displayValue="false"
                     jsbarcode-height="30"
                >
                </svg>
            </div>
        @endif
    </div>
</div>

<script type="text/javascript" src="<?php echo asset('public/vendor/jquery/jquery.min.js') ?>"></script>
<script src="<?php echo asset('public/js/JsBarcode.all.min.js') ?>"></script>
<script type="text/javascript">
JsBarcode(".barcode").init();
function auto_print() {
// window.print();
}
setTimeout(auto_print, 1000);
</script>
</body>
</html>
